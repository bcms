# BC CMS - Content Management System
# Copyright (c) 2008, Carlos Daniel Ruvalcaba Valenzuela
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
#    * Redistributions of source code must retain the above copyright notice, this 
#      list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright notice, this 
#      list of conditions and the following disclaimer in the documentation and/or other 
#      materials provided with the distribution.
#    * Neither the name of the BlackChair Software nor the names of its contributors may be 
#      used to endorse or promote products derived from this software without specific prior 
#      written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
# FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
# DATA, OR PROFITS; OR BUSINESS  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER 
# IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
# OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from django.db import models

class Languages(models.Model):
    class Admin:
        pass
    name = models.CharField(maxlength=4)
    
class Page(models.Model):
    class Admin:
        pass
    name = models.CharField(maxlength=80)
    lang = models.CharField(maxlength=4)
    published = models.DateTimeField()
    
class PageRevision(models.Model):
    class Admin:
        pass
    page = models.ForeignKey(Page)
    title = models.CharField(maxlength=255)
    contents = models.TextField()
    author = models.IntegerField()
    published = models.DateTimeField()

class Menu(models.Model):
    class Admin:
        pass
    title = models.CharField(maxlength=64)
    index = models.IntegerField()

class MenuLink(models.Model):
    class Admin:
        pass
    title = models.CharField(maxlength=64)
    url = models.CharField(maxlength=255)
    index = models.IntegerField()
    menu = models.ForeignKey(Menu)
    
class Upload(models.Model):
    class Admin:
        pass
    filename = models.CharField(maxlength=255)
    md5 = models.CharField(maxlength=32)
    filesize = models.IntegerField()
    filetype = models.CharField(maxlength=64)
    author = models.IntegerField()
    published = models.DateTimeField()
