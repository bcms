# BC CMS - Content Management System
# Copyright (c) 2008, Carlos Daniel Ruvalcaba Valenzuela
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, 
# are permitted provided that the following conditions are met:
#
#    * Redistributions of source code must retain the above copyright notice, this 
#      list of conditions and the following disclaimer.
#    * Redistributions in binary form must reproduce the above copyright notice, this 
#      list of conditions and the following disclaimer in the documentation and/or other 
#      materials provided with the distribution.
#    * Neither the name of the BlackChair Software nor the names of its contributors may be 
#      used to endorse or promote products derived from this software without specific prior 
#      written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR 
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND 
# FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR 
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, 
# DATA, OR PROFITS; OR BUSINESS  INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER 
# IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT 
# OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import datetime

from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django import newforms as forms
from django.conf import settings

from bcms.template import template_django as template

from models import *

def logmeout(request):
    logout(request)
    if request.GET.has_key('redir'):
        return HttpResponseRedirect(request.GET['redir'])
    return HttpResponseRedirect("/")
        
def logme(request):
    if request.method == 'POST':
        username = request.POST['username']
        passwd = request.POST['passwd']
        user = authenticate(username=username, password=passwd)
        if user is not None:
            login(request, user)
            if request.GET.has_key('redir'):
                return HttpResponseRedirect(request.GET['redir'])
            return HttpResponseRedirect("/")
        else:
            msg = "Username or password incorrect, please try again."
    msg = "Login"
    # Load the template
    template = loader.load('login.html')
    
    # Parse and execute the template with given variables
    stream = template.generate(msg=msg, user=request.user)
    
    # Return rendered template as HTML
    return HttpResponse(stream.render('html', doctype='html'))

@template('edit.html')
def editPage(request, lang,  pagename):
    if request.method == 'POST':
        title = request.POST['title']
        contents = request.POST['contents']
        print (title, contents)
        try:
            pg =  Page.objects.filter(name__iexact = pagename,  lang__iexact = lang)[0]
        except:
            pg = Page(name=pagename, lang=lang,  published=datetime.datetime.now())
            pg.save()
        p = pg.pagerevision_set.create(title=title, contents=contents, author=0, published=datetime.datetime.now())
        p.save()
        return HttpResponseRedirect("/%s/%s" % (lang,  pagename))

    try:
        pg = Page.objects.get(name__iexact = pagename,  lang__iexact = lang)
        view = pg.pagerevision_set.order_by("-published")[0]
        contents = view.contents
        title = view.title
    except:
        title = pagename
        contents = ''
    
    ptitle = 'Editing: ' + pagename
    args = dict(contents=contents, title=title, ptitle=ptitle, 
    	pagename=pagename, user=request.user)
    return args

@template('view.html')
def viewPage(request, lang,  pagename):
    if not pagename:
        pagename = 'index'
    try:
        pg = Page.objects.filter(name__iexact = pagename,  lang__iexact = lang)[0]
        view = pg.pagerevision_set.order_by("-published")[0]
    except:
        return HttpResponse("Requested page (%s) not Found!" %  pagename)
    
    args = dict(title = view.title, contents = view.contents, 
        pagename = pg.name,  lang = lang)
    return args
